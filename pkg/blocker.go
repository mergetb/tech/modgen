package modgen

type BlockFn func(b *Blocker) (BlockFn, *Block)

type Block struct {
	Items  []Item
	Blocks []*Block
}

func blockDelim(item Item) bool {
	return item.Type == Newline || item.Type == Indent || item.Type == Colon
}

type Continue struct {
	Comma, Curly, Paren bool
}

func (c Continue) Eval() bool {
	return c.Comma || c.Curly || c.Paren
}

type Blocker struct {
	Lexer    *Lexer
	Current  *Block
	Blocks   chan Block
	Error    error
	Continue Continue
}

func (b *Blocker) AddItem(item Item) {

	if !blockDelim(item) {
		b.Current.Items = append(b.Current.Items, item)
	}

}

func (b *Blocker) Block() *Block {

	x := b.Current
	b.Current = new(Block)
	return x

}

func StringBlocker(source string) *Blocker {
	return &Blocker{
		Lexer:   StringLexer(source),
		Blocks:  make(chan Block),
		Current: new(Block),
	}
}

func FileBlocker(filename string) *Blocker {
	return &Blocker{
		Lexer:   FileLexer(filename),
		Blocks:  make(chan Block),
		Current: new(Block),
	}
}

func (b *Blocker) Run() {

	go b.Lexer.Run()

	root := &Block{}
	var block *Block
	for blockfn := blockBegin; blockfn != nil; {
		blockfn, block = blockfn(b)
		if block != nil {
			root.Blocks = append(root.Blocks, block)
		}
	}
	b.Blocks <- *root
	close(b.Blocks)

}

func blockBegin(b *Blocker) (BlockFn, *Block) {

	for {

		item, ok := <-b.Lexer.Items

		if !ok {
			return nil, nil
		}

		b.AddItem(item)

		if item.Type == Import {
			return simpleBlock(b)
		}

		if item.Type == From {
			return simpleBlock(b)
		}

		if item.Type == Ident {
			return identBlock(b)
		}

	}

	return nil, nil

}

func simpleBlock(b *Blocker) (BlockFn, *Block) {

	for {
		item, ok := <-b.Lexer.Items
		if !ok {
			return nil, nil
		}

		b.AddItem(item)

		if item.Type == Newline {
			return blockBegin, b.Block()
		}
	}

	return nil, nil
}

func identBlock(b *Blocker) (BlockFn, *Block) {

	for {

		item, ok := <-b.Lexer.Items
		if !ok {
			return nil, nil
		}

		b.AddItem(item)

		if item.Type == Colon {
			return splineBlock(b)
		}

		if item.Type == OpenCurly {
			b.Continue.Curly = true
			return paramsBlock(b)
		}

	}

	return blockBegin, nil
}

func splineBlock(b *Blocker) (BlockFn, *Block) {

	sb := b.Block()

	// get to first actual block
	for {

		item, ok := <-b.Lexer.Items
		if !ok {
			return nil, nil
		}

		b.AddItem(item)

		// pass up newlines
		if item.Type == Newline {
			continue
		}

		// if we've reached indented code we've possibly found a block
		if item.Type == Indent {

			// plow through remaining indentation
			for item.Type == Indent {
				item, ok = <-b.Lexer.Items
				if !ok {
					return nil, nil
				}
			}

			itemBlock := splineSubBlockItem(b)
			if itemBlock != nil {
				sb.Blocks = append(sb.Blocks, itemBlock)
			}

			continue
		}

		// anything else (not indented) and the spline subblock is over
		return identBlock, sb

	}

	return nil, sb

}

func checkContinue(b *Blocker, item Item) bool {

	switch item.Type {

	case Comma:
		b.Continue.Comma = true
		return true

	case OpenCurly:
		b.Continue.Curly = true
		return true

	case CloseCurly:
		b.Continue.Curly = false
		return true

	case OpenParen:
		b.Continue.Paren = true
		return true

	case CloseParen:
		b.Continue.Paren = false
		return true

	}

	return false

}

func splineSubBlockItem(b *Blocker) *Block {

	for {

		item, ok := <-b.Lexer.Items
		if !ok {
			return nil
		}

		b.AddItem(item)

		if item.Type == Newline && !b.Continue.Eval() {
			return b.Block()
		}

		if checkContinue(b, item) {
			continue
		}

		b.Continue.Comma = false

	}

}

func paramsBlock(b *Blocker) (BlockFn, *Block) {

	return blockBegin, splineSubBlockItem(b)

}
