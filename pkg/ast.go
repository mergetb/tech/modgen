package modgen

type Program struct {
	Imports         []ImportStmt
	Shorthands      []Shorthand
	SplineDefs      []SplineDef
	SplineInstances []SplineInstance
	Reticulators    []Reticulator
}

type ImportStmt struct {
	Packages []string
}

type Shorthand struct {
	Package string
	Splines []string
}

type SplineDef struct {
	Name            string
	SplineInstances []SplineInstance
	Variables       []Variable
	Parameters      []Parameter
	Equations       []Equation
	Reticulations   []Reticulation
}

type Reticulator struct {
	Args       []Arg
	Statements []ReticulatorStatement
}

type ReticulatorStatement struct {
	SplineInstance *SplineInstance
	Reticulation   *Reticulation
	Equation       *Equation
	Code           *CodeStatement
}

type SplineInstance struct {
	Type              string
	Name              string
	ParameterBindings []ParameterBinding
}

type Variable struct {
	Type string
	Name string
}

type Parameter struct {
	Type string
	Name string
}

type ParameterBinding struct {
	Name  string
	Value ParameterValue
}

type ParameterValue struct {
	StringValue *string
	IntValue    *int
	FloatValue  *float64
	Unit        *Unit
}

type Unit ItemType

type Arg struct {
	Type string
	Name string
}

type CodeStatement struct {
	//TODO
}

type Equation struct {
	//TODO
}

type Reticulation struct {
	SplineRefs []string
}
