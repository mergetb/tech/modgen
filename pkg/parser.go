package modgen

type ParseFn func(p *Parser) ParseFn

type Parser struct {
	Blocker *Blocker
}
