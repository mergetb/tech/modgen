/*
 This is the ModGen lexer. It's design is based on the following talk by Rob Pike

 	- https://www.youtube.com/watch?v=HxaD_trXwRE

 The following are the tokens produced by this lexer.

 TOKEN                  Examples
 ==============================================================================
 ----------------
 | Alphanumeric
 ----------------
 Ident                  x, stepUp, λ, step_up, block47, _error
 Import                 import
 From                   from
 Use                    use
 Reticulate             reticulate
 Byte                   B
 Kilobyte               KB
 Megabyte               MB
 Gigabyte               GB
 Terabyte               TB
 Petabyte               PB
 Exabyte                EB
 Bit                    b
 Kilobit                Kb
 Megabit                Mb
 Gigabit                Gb
 Terabit                Tb
 Petabit                Pb
 Exabit                 Eb
 BytesPerSecond         BPS
 KilobytesPerSecond     KBPS
 MegabytesPerSecond     MBPS
 GigabytesPerSecond     GBPS
 TerabytesPerSecond     TBPS
 PetabytesPerSecond     PBPS
 ExabytesPerSecond      EBPS
 BitsPerSecond          bps
 KilobitsPerSecond      kbps
 MegabitsPerSecond      mbps
 GigabitsPerSecond      gbps
 TerabitsPerSecond      tbps
 PetabitsPerSecond      pbps
 ExabitsPerSecond       ebps
 Picoseconds            ps
 Nanoseconds            ns
 Microseconds           us, μs
 Milliseconds           ms
 Seconds                s
 Time                   t
 ----------------
 | Numeric
 ----------------
 Integer                47
 Float                  4.7, 4.7e-3
 ----------------
 | Special
 ----------------
 OpenParen              (
 CloseParen             )
 OpenCurly              {
 CloseCurly             }
 OpenSquare             [
 CloseSquare            ]
 Prime                  '
 String                 "do you know the muffin man?", `the muffin man?`
 Dot                    .
 Comma                  ,
 Colon                  :
 Equals                 =
 Greater                >
 GreaterEqual           >=
 Less                   <
 LessEqual              <=
 Not                    !
 Reti                   ~
 Minus                  -
 Plus                   +
 PlusEqual              +=
 Mul                    *
 Div                    /
 Pow                    ^
 Newline                \n, \r
 Indent                 ^  , ^\t
 EOF                    <end of file>
*/

package modgen

import (
	"fmt"
	"io/ioutil"
	"unicode"
)

type ItemType int
type LexFn func(l *Lexer) LexFn

const (
	Undefined ItemType = iota
	Ident
	Import
	From
	Use
	Reticulate
	Byte
	Kilobyte
	Megabyte
	Gigabyte
	Terabyte
	Petabyte
	Exabyte
	Bit
	Kilobit
	Megabit
	Gigabit
	Terabit
	Petabit
	Exabit
	BytesPerSecond
	KilobytesPerSecond
	MegabytesPerSecond
	GigabytesPerSecond
	TerabytesPerSecond
	PetabytesPerSecond
	ExabytesPerSecond
	BitsPerSecond
	KilobitsPerSecond
	MegabitsPerSecond
	GigabitsPerSecond
	TerabitsPerSecond
	PetabitsPerSecond
	ExabitsPerSecond
	Picoseconds
	Nanoseconds
	Microseconds
	Milliseconds
	Seconds
	Time
	Integer
	Float
	OpenParen
	CloseParen
	OpenCurly
	CloseCurly
	OpenSquare
	CloseSquare
	Prime
	String
	Dot
	Comma
	Colon
	Equals
	Greater
	GreaterEqual
	Less
	LessEqual
	Not
	Reti
	Minus
	Plus
	PlusEqual
	Mul
	Div
	Pow
	Newline
	Indent
	EOF
)

func (x ItemType) String() string {

	switch x {
	case Undefined:
		return "?"
	case Ident:
		return "Ident"
	case Import:
		return "Import"
	case From:
		return "From"
	case Use:
		return "Use"
	case Reticulate:
		return "Reticulate"
	case Byte:
		return "Byte"
	case Kilobyte:
		return "Kilobyte"
	case Megabyte:
		return "Megabyte"
	case Gigabyte:
		return "Gigabyte"
	case Terabyte:
		return "Terabyte"
	case Petabyte:
		return "Petabyte"
	case Exabyte:
		return "Exabyte"
	case Bit:
		return "Bit"
	case Kilobit:
		return "Kilobit"
	case Megabit:
		return "Megabit"
	case Gigabit:
		return "Gigabit"
	case Terabit:
		return "Terabit"
	case Petabit:
		return "Petabit"
	case BytesPerSecond:
		return "BytesPerSecond"
	case KilobytesPerSecond:
		return "KilobytesPerSecond"
	case MegabytesPerSecond:
		return "MegabytsPerSecond"
	case GigabytesPerSecond:
		return "GigabytesPerSecond"
	case TerabytesPerSecond:
		return "TerabytesPerSecond"
	case PetabytesPerSecond:
		return "PetabytesPerSecond"
	case ExabytesPerSecond:
		return "ExabytesPerSecond"
	case BitsPerSecond:
		return "BitsPerSecond"
	case KilobitsPerSecond:
		return "KilobitsPerSecond"
	case MegabitsPerSecond:
		return "MegabitsPerSecond"
	case GigabitsPerSecond:
		return "GigabitsPerSecond"
	case TerabitsPerSecond:
		return "TerabitsPerSecond"
	case PetabitsPerSecond:
		return "PetabitsPerSecond"
	case ExabitsPerSecond:
		return "ExabitsPerSecond"
	case Picoseconds:
		return "Picoseconds"
	case Nanoseconds:
		return "Nanoseconds"
	case Microseconds:
		return "Microseconds"
	case Milliseconds:
		return "Milliseconds"
	case Seconds:
		return "Seconds"
	case Time:
		return "Time"
	case Integer:
		return "Integer"
	case Float:
		return "Float"
	case OpenParen:
		return "OpenParen"
	case CloseParen:
		return "CloseParen"
	case OpenCurly:
		return "OpenCurly"
	case CloseCurly:
		return "CloseCurly"
	case OpenSquare:
		return "OpenSquare"
	case CloseSquare:
		return "CloseSquare"
	case Prime:
		return "Prime"
	case String:
		return "String"
	case Dot:
		return "Dot"
	case Comma:
		return "Comma"
	case Colon:
		return "Colon"
	case Equals:
		return "Equals"
	case Greater:
		return "Greater"
	case GreaterEqual:
		return "GreaterEqual"
	case Less:
		return "Less"
	case LessEqual:
		return "LessEqual"
	case Not:
		return "Not"
	case Reti:
		return "Reti"
	case Minus:
		return "Minus"
	case Plus:
		return "Plus"
	case PlusEqual:
		return "PlusEqual"
	case Mul:
		return "Mul"
	case Div:
		return "Div"
	case Pow:
		return "Pow"
	case Newline:
		return "Newline"
	case Indent:
		return "Indent"
	case EOF:
		return "EOF"
	}

	return "?"

}

type Item struct {
	Type      ItemType
	Text      string
	Line, Col int
}

func (i Item) String() string {
	return fmt.Sprintf("%v %q %d:%d", i.Type, i.Text, i.Line, i.Col)
}

type Lexer struct {
	Input      []rune
	Start, Pos int
	Line, Col  int
	Items      chan Item
	Error      error
}

func (l *Lexer) Emit(t ItemType) {
	l.Items <- Item{
		Type: t,
		Line: l.Line,
		Col:  l.Col - (l.Pos - l.Start),
		Text: l.CurrentText(),
	}
	l.Start = l.Pos
}

func (l *Lexer) CurrentText() string {

	return string(l.Input[l.Start:l.Pos])

}

func (l *Lexer) Run() {
	for state := lexBegin; state != nil; {
		state = state(l)
	}
	close(l.Items)
}

func (l *Lexer) Next() bool {

	l.Pos++
	l.Col++

	if l.Pos >= len(l.Input) {
		return false
	}

	return true
}

func (l *Lexer) Consume() bool {

	ret := l.Next()
	l.Start = l.Pos
	return ret

}

func lexBegin(l *Lexer) LexFn {

	for l.Pos < len(l.Input) {

		switch {
		case unicode.IsLetter(l.Input[l.Pos]) || l.Input[l.Pos] == '_':
			return lexAlphanumeric(l)
		case isNumber(l):
			return lexNumeric(l)
		default:
			return lexSpecial(l)
		}

	}

	if l.Pos == len(l.Input) {
		l.Emit(EOF)
	}
	return nil

}

func isNumber(l *Lexer) bool {

	switch {
	case unicode.IsNumber(l.Input[l.Pos]):
		return true
	case l.Input[l.Pos] == '-' && unicode.IsNumber(l.Input[l.Pos+1]):
		return true
	}

	return false

}

func lexAlphanumeric(l *Lexer) LexFn {

	for unicode.IsLetter(l.Input[l.Pos]) || unicode.IsNumber(l.Input[l.Pos]) ||
		l.Input[l.Pos] == '_' {
		if !l.Next() {
			break
		}
	}

	text := string(l.Input[l.Start:l.Pos])
	typ := Ident
	switch text {
	case "import":
		typ = Import
	case "from":
		typ = From
	case "use":
		typ = Use
	case "reticulate":
		typ = Reticulate
	case "b":
		typ = Bit
	case "kb":
		typ = Kilobit
	case "mb":
		typ = Megabit
	case "gb":
		typ = Gigabit
	case "tb":
		typ = Terabit
	case "pb":
		typ = Petabit
	case "eb":
		typ = Exabit
	case "B":
		typ = Byte
	case "KB":
		typ = Kilobyte
	case "MB":
		typ = Megabyte
	case "GB":
		typ = Gigabyte
	case "TB":
		typ = Terabyte
	case "PB":
		typ = Petabyte
	case "EB":
		typ = Exabyte
	case "BPS":
		typ = BytesPerSecond
	case "KBPS":
		typ = KilobytesPerSecond
	case "MBPS":
		typ = MegabytesPerSecond
	case "GBPS":
		typ = GigabytesPerSecond
	case "TBPS":
		typ = TerabytesPerSecond
	case "PBPS":
		typ = PetabytesPerSecond
	case "EBPS":
		typ = ExabytesPerSecond
	case "bps":
		typ = BitsPerSecond
	case "kbps":
		typ = KilobitsPerSecond
	case "mbps":
		typ = MegabitsPerSecond
	case "gbps":
		typ = GigabitsPerSecond
	case "tbps":
		typ = TerabitsPerSecond
	case "pbps":
		typ = PetabitsPerSecond
	case "ebps":
		typ = ExabitsPerSecond
	case "ps":
		typ = Picoseconds
	case "ns":
		typ = Nanoseconds
	case "us", "μs":
		typ = Microseconds
	case "ms":
		typ = Milliseconds
	case "s":
		typ = Seconds
	case "t":
		typ = Time
	}

	l.Emit(typ)
	return lexBegin(l)

}

func lexNumeric(l *Lexer) LexFn {

	if l.Input[l.Pos] == '-' {
		l.Next()
	}

	typ := Integer
	for unicode.IsNumber(l.Input[l.Pos]) || l.Input[l.Pos] == '.' {
		if l.Input[l.Pos] == '.' {
			typ = Float
		}
		if !l.Next() {
			break
		}
	}

	l.Emit(typ)
	return lexBegin(l)

}

func lexSpecial(l *Lexer) LexFn {

	switch l.Input[l.Pos] {

	case ' ', '\t':
		return lexSpace(l)

	case '(':
		l.Next()
		l.Emit(OpenParen)
		return lexBegin(l)

	case ')':
		l.Next()
		l.Emit(CloseParen)
		return lexBegin(l)

	case '{':
		l.Next()
		l.Emit(OpenCurly)
		return lexBegin(l)

	case '}':
		l.Next()
		l.Emit(CloseCurly)
		return lexBegin(l)

	case '[':
		l.Next()
		l.Emit(OpenSquare)
		return lexBegin(l)

	case ']':
		l.Next()
		l.Emit(CloseSquare)
		return lexBegin(l)

	case '\'':
		l.Next()
		l.Emit(Prime)
		return lexBegin(l)

	case '"':
		l.Next()
		return lexString(l)

	case '.':
		l.Next()
		l.Emit(Dot)
		return lexBegin(l)

	case ',':
		l.Next()
		l.Emit(Comma)
		return lexBegin(l)

	case ':':
		l.Next()
		l.Emit(Colon)
		return lexBegin(l)

	case '=':
		l.Next()
		l.Emit(Equals)
		return lexBegin(l)

	case '>':
		l.Next()
		if l.Input[l.Pos+1] == '=' {
			l.Next()
			l.Emit(GreaterEqual)
			return lexBegin(l)
		}
		l.Emit(Greater)
		return lexBegin(l)

	case '<':
		l.Next()
		if l.Input[l.Pos+1] == '=' {
			l.Next()
			l.Emit(LessEqual)
			return lexBegin(l)
		}
		l.Emit(Less)
		return lexBegin(l)

	case '!':
		l.Next()
		l.Emit(Not)
		return lexBegin(l)

	case '~':
		l.Next()
		l.Emit(Reti)
		return lexBegin(l)

	case '-':
		l.Next()
		l.Emit(Minus)
		return lexBegin(l)

	case '+':
		l.Next()
		if l.Input[l.Pos+1] == '=' {
			l.Next()
			l.Emit(PlusEqual)
			return lexBegin(l)
		}
		l.Emit(Plus)
		return lexBegin(l)

	case '*':
		l.Next()
		l.Emit(Mul)
		return lexBegin(l)

	case '/':
		l.Next()
		if l.Input[l.Pos] == '/' {
			l.Next()
			return lexLineComment(l)
		}
		l.Emit(Div)
		return lexBegin(l)

	case '^':
		l.Next()
		l.Emit(Pow)
		return lexBegin(l)

	case '\r', '\n':
		eof := !l.Next()
		l.Emit(Newline)
		if !eof {
			l.Line++
			l.Col = 1
		}
		return lexBegin(l)

	}

	l.Error = fmt.Errorf(
		"unexpected %q at %d:%d", string(l.Input[l.Pos]), l.Line, l.Col)

	return nil

}

func lexString(l *Lexer) LexFn {

	for l.Input[l.Pos] != '"' {
		l.Next()
		if l.Pos >= len(l.Input) {
			l.Error = fmt.Errorf("EOF reached while reading string at %d:%d", l.Line, l.Col)
			return nil
		}
	}
	l.Next()
	l.Emit(String)
	return lexBegin(l)

}

func lexLineComment(l *Lexer) LexFn {

	for l.Input[l.Pos] != '\n' {
		if !l.Next() {
			break
		}
	}
	l.Start = l.Pos
	return lexBegin(l)

}

func lexSpace(l *Lexer) LexFn {

	if l.Col == 1 {
		return lexIndent(l)
	}
	l.Consume()
	return lexBegin(l)

}

func lexIndent(l *Lexer) LexFn {

	for l.Input[l.Pos] == ' ' || l.Input[l.Pos] == '\t' {
		if !l.Next() {
			break
		}
	}
	l.Emit(Indent)
	return lexBegin(l)

}

func StringLexer(source string) *Lexer {
	return &Lexer{
		Line:  1,
		Col:   1,
		Items: make(chan Item),
		Input: []rune(string(source)),
	}
}

func FileLexer(filename string) *Lexer {

	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	return &Lexer{
		Line:  1,
		Col:   1,
		Items: make(chan Item),
		Input: []rune(string(buf)),
	}

}
