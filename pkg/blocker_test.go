package modgen

import (
	"github.com/davecgh/go-spew/spew"
	"testing"
)

func testBlockFile(t *testing.T, filename string) {

	b := FileBlocker(filename)
	go b.Run()

	var blocks []Block
	for {

		block, ok := <-b.Blocks
		if ok {
			blocks = append(blocks, block)
		} else {
			break
		}

	}

	for _, x := range blocks {
		t.Log(spew.Sdump(x))
	}

	if b.Error != nil {
		t.Fatal(b.Error)
	}

}

func TestWidgetBlock(t *testing.T) {

	testBlockFile(t, "../test/widget.mg")

}

func TestThreeBusBlock(t *testing.T) {

	testBlockFile(t, "../test/threebus.mg")

}

func TestTwoBusBlock(t *testing.T) {

	testBlockFile(t, "../test/twobus.mg")

}
