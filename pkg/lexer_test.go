package modgen

import (
	"testing"
)

func testLexFile(t *testing.T, filename string) {

	l := FileLexer(filename)
	go l.Run()

	var tokens []Item
	for {
		item, more := <-l.Items
		if more {
			tokens = append(tokens, item)
		} else {
			break
		}
	}

	for _, x := range tokens {
		t.Log(x)
	}

	if l.Error != nil {
		t.Fatal(l.Error)
	}

}

func TestWidgetLex(t *testing.T) {

	testLexFile(t, "../test/widget.mg")

}

func TestThreeBusLex(t *testing.T) {

	testLexFile(t, "../test/threebus.mg")

}

func TestTwoBusLex(t *testing.T) {

	testLexFile(t, "../test/twobus.mg")

}
