// My super cool widget
Widget:
  var x,y
  SubComponent Q{ λ = 47 }
  Combobulator C{
    ω <= 17,
    mode ! "recombobulate"
  }

  y = m*x + b
  5*x = 4

  Q.λ ~ C.mode ~ y

Nugget:

  Element Au { Name = "Gold", 
               Number = 79 },
          Ag { Name = "Silver", 
               Number = 47 }

  Au.Number = Ag.Number + 32

