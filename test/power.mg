module power

// A terminal is an connection point between elements
Terminal:
  
  // potential and current
  var i,e


// A bus is a multi-terminal interconnection point between elements
Bus:

  // A bus has a set of terminals as interconnection points
  Terminal[] T

  // KCL and voltage equivalence
  0 = sum x.i in T 
  T[0].e = T[:].e

// An EmGen is an emulated generator. This element represents the behavior of a
// simple power generator that produces a voltage at it's terminals that is
// proportional to prime mover input energy minus the load current scaled by a
// constant factor.
EmGen:
  // Interconnection point
  Terminal  T

  // Prime mover input energy
  var λ

  // Load friction constant and rotational frequency
  real k,ω
 
  // Output voltage has a magnitude that is proportional to the prime mover
  // input energy minus the load current scaled by the load friction constant.
  T.e = (λ - k*T.i)*sin(ω*t)

// An EmLoad is an emulated load. This element represents the behavior of a load
// by drawing a constant current.
EmLoad:
  // Interconnection point
  Terminal T

  // Current draw
  var z

  // Draw current from terminal
  z = T.i

// A TLine is a simple transmission line model.
TLine:

  // The two terminal interconnection points.
  [2]Terminal T

  // Resistance of the line
  real R


  // Current is constant through the line
  T[0].i = T[1].i

  // Basic ohms law voltage drop across the line
  i := T[0].i
  v := T[0].e - T[1].e
  v = i*R

// Connect two terminals together by setting their currents and potentials as
// equal.
reticulate(Terminal a, b):

  a.i = b.i
  a.e = b.e

// Connect an element type T to a bus. T must have a terminal component called
// T.
reticulate[T](T x, Bus b):

  Terminal T{}
  x.T ~ T
  b.T += T
 
// Connect a bus to the 'left' (first) terminal of a transmissionline
reticulate(Bus b, TLine x):

  Terminal T{}
  x.T[0] ~ T
  b.T += T

// Connect a bus to the 'right' (second) terminal of a transmissionline
reticulate(TLine x, Bus b):

  Terminal T{}
  x.T[1] ~ T
  b.T += T
