module cyber

// Computer ===================================================================

Proc:
  uint cores
  real frequency

MemoryModule:
  uint capcity
  uint datarate

Computer:
  Endpoint[]     endpoints
  Proc[]         CPU
  MemoryModule[] memory


// Network ====================================================================

Endpoint:
  uint latency
  uint capacity

Link:
  Endpoint[] endpoints
  uint latency
  uint capacity

reticulate<T>(T x, Link L, T[] xs):

  Endpoint xe
  Endpoint[len(xs)] xes
  Endpoint tes[len(xs)+1]

  xe = tes[0]
  xes[:] = tes[1:]

  x.endpoints += xe
  xs.endpoints += xes
  l.endpoints += tes

