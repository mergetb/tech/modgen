import power, cyber
from power use EmGen, EmLoad, Bus, TLine

TwoBus:

    // Physical Components

    EmGen  g{ k = 1e-3, ω = 60*2*π }
    TLine  t{ R = 0.5 }
    Bus    b0, b1
    EmLoad l{ z = 100 }

    // Physical connections
    g ~ b0 ~ t ~ b1 ~ l

    // Sensors
    Sensor   v_sense{ rate = 1000, tag = 100, target = "localhost" }

    v_sense ~ b0.T[0].v

    // Actuators

    Actuator gen_act { static = [0, 10],  dynamic = [-0.5, 0.5], tag = 101 },
             load_act{ static = [0, 100], dynamic = [-0.5, 0.5], tag = 102 }

    gen_act ~ g.λ
    load_act ~ Load.z

    // Cyber components
    Computer gen_ctl, v_mon, load_ctl{
        CPU =    [1]{ cores = 2, frequency = 2 GHz }
        Memory = [2]{ capacity = 1 GB, datarate = 2500 MTs }
    }
    Link internet { latency = 5 ms, capacity = 10 mbps },
         lan1,lan2,lan3 { capacity = 1 gbps }
