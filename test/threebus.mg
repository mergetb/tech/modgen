import power, cyber
from power use EmGen, EmLoad, Bus, TLine

ThreeBus:

  // Physical Elements

  EmGen    G0{ k = 4e-3, ω = 60*2*Pi },
           G1{ k = 7e-3, ω = 60*2*Pi }
  EmLoad   Load{ z = 100 }
  Bus      A, 
           B, 
           C
  TLine    TAC{ R = 0.5 },
           TBC{ R = 0.8 }

  // Physical Connections

  G0 ~ A ~ TAC ~ B ~ Load
  G1 ~ B ~ TBC ~ C ~ Load

  // Sensors and Actuators

  Sensor sg0{ rate = 1000, tag = 100, target = "localhost" },
         sg1{ rate = 1000, tag = 101, target = "localhost" }

  sg0 ~ G0.T.e
  sg1 ~ G0.T.e

  Actuator ag0{ static = [0,10],  dynamic = [-0.5,0.5], tag = 100},
           ag1{ static = [0,10],  dynamic = [-0.5,0.5], tag = 101},
            al{ static = [0,100], dynamic = [-0.5,0.5], tag = 102}

  ag0 ~ G0.λ
  ag1 ~ G1.λ
  al  ~ Load.z

  // Cyber Elements

  Computer gc0, gc1, lc{
    CPU    = [1]{ cores >= 2, cores <= 4, frequency >= 2 GHz },
    Memory = [2]{ capacity >= 2 GB, datarate >= 2400 MTs }
  }

  Link internet { latency < 7 ms,   capacity > 10 mbps },
       lan0,lan1{ latency < 100 us, capacity > 1 gbps  }

  gc0 ~ lan0 ~ ag0,sg0
  gc1 ~ lan1 ~ ag1,sg1
  gc0 ~ internet ~ gc1

  
// Simulation Parameters

Sim {
  object = ThreeBus,
  begin  = 0, 
  end    = 10, 
  step   = 1e-3 
}

