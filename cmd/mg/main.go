package main

import (
	"log"

	"gitlab.com/mergetb/tech/modgen/pkg"
)

var src = `
// My super cool widget
Widget:
  var x,y
  SubComponent Q{ λ = 47 tbps }
  Combobulator C{
    ω <= -17.2,
    mode ! "recombobulate"
  }

  y = m*x + b
  5*x = 4

  Q.λ ~ C.mode ~ y
`

func main() {

	log.SetFlags(0)

	lexer := modgen.StringLexer(src)
	go lexer.Run()

	for {
		item, more := <-lexer.Items
		if more {
			log.Printf("%+v", item)
		} else {
			break
		}
	}

	if lexer.Error != nil {
		log.Fatal(lexer.Error)
	}
	log.Printf("done!")

}
